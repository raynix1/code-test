# gem install http
require 'http'

URL = ARGV[0] || "https://gitlab.com"
SECONDS = ARGV[1].to_i() || 300

def process_clock()
  return Process.clock_gettime(Process::CLOCK_MONOTONIC)
end

def http_bench(url)
  http_start = process_clock()
  response = HTTP.follow(max_hops:5).get(url)
  http_end = process_clock()
  return [ response.status.code, http_end - http_start ]
end

puts "Checking #{URL} for #{SECONDS} seconds..."
start_time = process_clock()
results = []
until process_clock() > start_time + SECONDS do
  print "."
  results.append(http_bench(URL))
end
puts "Done"

status_codes = results.map{|x| x[0]}.uniq
status_codes.each do |s|
  grouped_by = results.map{|x| x[0] == s ? x[1] : nil}
  total = grouped_by.sum(0.0)
  count = grouped_by.size
  avg = total / count
  puts "status #{s} count: #{count}, average: #{avg.round(4)} seconds"
end
