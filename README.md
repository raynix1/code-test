# code-test

Code for my GitLab test

## Usage

By default this script will check https://gitlab.com for 5 minutes and print out response times grouped by status codes.

```bash
$ ruby http-bench.rb 
Checking https://gitlab.com for 300 seconds...
................................................Done
status 200 count: 466, average: 0.6427 seconds
```

URL and duration can be set via command line ARGVs

```bash
$ ruby http-bench.rb https://raynix.info 60
```

